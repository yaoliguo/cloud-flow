package pres.lg.flow.cloudflow.modular.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import pres.lg.flow.cloudflow.modular.system.entity.Employee;

@Mapper
public interface EmployeeMapper {

    @Select("SELECT * FROM employee WHERE id = #{id}")
    Employee getById(Long id);

}
