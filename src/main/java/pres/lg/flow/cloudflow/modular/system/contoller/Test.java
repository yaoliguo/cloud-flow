package pres.lg.flow.cloudflow.modular.system.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pres.lg.flow.cloudflow.modular.system.entity.Employee;
import pres.lg.flow.cloudflow.modular.system.mapper.EmployeeMapper;

@RestController
public class Test {

    @Autowired
    EmployeeMapper employeeMapper;

    @RequestMapping("/sdddddd")
    public Object test(){

        Employee emp = employeeMapper.getById(1l);

        return emp;
    }

}
