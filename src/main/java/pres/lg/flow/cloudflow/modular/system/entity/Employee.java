package pres.lg.flow.cloudflow.modular.system.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Employee {

    private int id;

    private String name;

    private int sex;

    private Date birthday;

    private String contactCode;

    private int del;


}
